package com.the.sample.app.service;

import com.the.sample.app.model.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {
    Flux<User> findAll();
    Mono<User> findById(String id);
    Mono<User> findByEmail(String email);
    void save(User user);
    void deleteById(String id);
}
