package com.the.sample.app.controller;

import com.the.sample.app.model.User;
import com.the.sample.app.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserRestController {
    private final UserService userService;

    @GetMapping("/")
    public Flux<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<User> findUserById(@PathVariable("id") String id) {
        return userService.findById(id);
    }


    @PostMapping("/")
    public Mono<User> saverUser(@RequestBody User user) {
        userService.save(user);
        return Mono.just(user);
    }

    @PutMapping("/{id}")
    public Mono<User> updateUser(@PathVariable("id") String id,@RequestBody User user) {
        user.setId(id);
        userService.save(user);
        return Mono.just(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") String id) {
        userService.deleteById(id);
    }
}
